﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using YaWeatherClient.Api.Common.Cache;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Entities.Models;
using YaWeatherClient.Api.Enums;
using YaWeatherClient.Api.Services.Yandex;

namespace YaWeatherClient.Api.Features.GetWeather
{
    public static partial class GetWeather
    {
        public class Handler : IRequestHandler<Query, AppWeatherResponseDTO>
        {
            private readonly GeoCodeCache _geoCodeCache;
            private readonly YandexProvider _yandexProvider;
            private readonly ReceivedWeatherCache _receivedWeatherCache;
            private readonly IMapper _mapper;
            private readonly IOptions<YandexOptions> _options;

            public Handler(GeoCodeCache geoCodeCache, ReceivedWeatherCache receivedWeatherCache,
                            YandexProvider yandexProvider, IMapper mapper, IOptions<YandexOptions> options)
            {
                _geoCodeCache = geoCodeCache;
                _yandexProvider = yandexProvider;
                _receivedWeatherCache = receivedWeatherCache;
                _mapper = mapper;
                _options = options;
            }

            public async Task<AppWeatherResponseDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                City targetCity = (City)Enum.Parse(typeof(City), request.City, true);
                var storedWeather = _receivedWeatherCache.GetByCity(targetCity);

                if (storedWeather == null)
                {
                    var receivedWeatherLine = new ReceivedWeatherLine()
                    {
                        city = targetCity,
                        yaWeather = await _yandexProvider.TestWeather(_geoCodeCache.GetByCity(targetCity))
                    };
                    _receivedWeatherCache.Store(receivedWeatherLine, TimeSpan.FromMinutes(_options.Value.CacheLifeTime));

                    return _mapper.Map<AppWeatherResponseDTO>(receivedWeatherLine.yaWeather.fact);
                }

                return _mapper.Map<AppWeatherResponseDTO>(storedWeather.fact);
            }
        }
    }
}
