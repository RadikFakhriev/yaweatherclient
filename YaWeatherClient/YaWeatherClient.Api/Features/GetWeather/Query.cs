﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;

namespace YaWeatherClient.Api.Features.GetWeather
{
    public static partial class GetWeather
    {
        public class Query : IRequest<AppWeatherResponseDTO>
        {
            public string City { get; set; }
        }
    }
}
