using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using YaWeatherClient.Api.Common;
using YaWeatherClient.Api.Common.Cache;
using YaWeatherClient.Api.Contexts;
using YaWeatherClient.Api.Entities.DB;
using YaWeatherClient.Api.Entities.Models;
using YaWeatherClient.Api.Interfaces;
using YaWeatherClient.Api.Middlewares;
using YaWeatherClient.Api.PostgreSQL.Repositories;
using YaWeatherClient.Api.Services.Yandex;

namespace YaWeatherClient.Api
{
    public class Startup
    {
        private string ConnectionString;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ReceivedWeatherCache>();
            services.AddSingleton<ObjectPool>();
            services.AddSingleton<GeoCodeCache>();
            services.AddScoped<YandexProvider>();
            services.AddTransient<IIncomingRequestRepository, IncomingRequestRepository>();

            services.AddHttpClient();
            services.AddHttpClient<YandexApiHttpClientService>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "YaWeatherClient.Api", Version = "v1" });
            });

            ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddTransient<AppWeatherDbContext>();
            services.AddDbContext<AppWeatherDbContext>(options =>
                        options.UseNpgsql(ConnectionString), ServiceLifetime.Transient);
            AppConnectionString.ConnectionString = ConnectionString;

            services.AddMemoryCache();
            services.AddLogging();

            services.Configure<YandexOptions>(Configuration.GetSection(YandexOptions.YandexApi));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "YaWeatherClient.Api v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
