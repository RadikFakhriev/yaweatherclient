﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DB;

namespace YaWeatherClient.Api.Interfaces
{
    public interface IIncomingRequestRepository
    {
        Task AddAsync(IncomingRequest incRequest, CancellationToken cancellationToken);
        Task UpdateReponseAsync(Guid requestId, int status, string responseBody, CancellationToken cancellationToken);
        Task InsertBulk(List<IncomingRequest> incRequests);
        Task AddRange(List<IncomingRequest> incRequests);
    }
}
