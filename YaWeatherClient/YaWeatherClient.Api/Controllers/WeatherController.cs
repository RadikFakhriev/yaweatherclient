﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Features.GetWeather;

namespace YaWeatherClient.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IMediator _mediator;

        public WeatherController (IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<AppWeatherResponseDTO> GetAsync([FromQuery] GetWeather.Query query)
            => await _mediator.Send(query);
    }
}
