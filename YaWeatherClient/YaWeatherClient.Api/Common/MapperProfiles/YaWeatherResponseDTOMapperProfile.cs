﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;

namespace YaWeatherClient.Api.Common.MapperProfiles
{
    public class YaWeatherResponseDTOMapperProfile : Profile
    {
        private readonly ImmutableDictionary<string, string> ConditionCyrillic = (new Dictionary<string, string>()
        {
            { "clear", "ясно" },
            { "partly-cloudy", "малооблачно" },
            { "cloudy", "облачно с прояснениями" },
            { "overcast", "пасмурно" },
            { "drizzle", "морось" },
            { "light-rain", "небольшой дождь" },
            { "rain", "дождь" },
            { "moderate-rain", "умеренно сильный дождь" },
            { "heavy-rain", "сильный дождь" },
            { "continuous-heavy-rain", "длительный сильный дождь"},
            { "showers", "ливень" },
            { "wet-snow", "дождь со снегом" },
            { "light-snow", "небольшой снег" },
            { "snow", "снег" },
            { "snow-showers", "снегопад" },
            { "hail", "град" },
            { "thunderstorm", "гроза" },
            { "thunderstorm-with-rain", "дождь с грозой" },
            { "thunderstorm-with-hail", "гроза с градом" }
        }).ToImmutableDictionary();

        private readonly ImmutableDictionary<string, string> WindDirCyrillic = (new Dictionary<string, string>()
        {
            { "nw", "северо-западное" },
            { "n",  "северное" },
            { "ne", "северо-восточное" },
            { "e", "восточное" },
            { "se", "юго-восточное" },
            { "s", "южное" },
            { "sw", "юго-западное" },
            { "w", "западное" },
            { "с", "штиль" }
        }).ToImmutableDictionary();

        private readonly ImmutableDictionary<string, string> SeasonCyrillic = (new Dictionary<string, string>()
        {
            { "summer", "лето" },
            { "autumn", "осень" },
            { "winter", "зима" },
            { "spring", "весна" }
        }).ToImmutableDictionary();

        public YaWeatherResponseDTOMapperProfile()
        {
            CreateMap<Fact, AppWeatherResponseDTO>()
                .ForMember(
                    dest => dest.Condition,
                    opt => opt.MapFrom(
                        src => ConditionCyrillic[src.Condition]))
                .ForMember(
                    dest => dest.WindDir,
                    opt => opt.MapFrom(
                        src => WindDirCyrillic[src.WindDir]))
                .ForMember(
                    dest => dest.Season,
                    opt => opt.MapFrom(
                        src => SeasonCyrillic[src.Season]));
        }
    }
}
