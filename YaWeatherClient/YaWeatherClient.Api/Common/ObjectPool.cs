﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DB;
using YaWeatherClient.Api.Interfaces;

namespace YaWeatherClient.Api.Common
{
    public class ObjectPool
    {
        private readonly ConcurrentBag<IncomingRequest> items = new ConcurrentBag<IncomingRequest>();
        private int counter = 0;
        private int MAX = 100000;
        private readonly IIncomingRequestRepository _requestRepository;

        public ObjectPool(IIncomingRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
            Interval.Set(async() => {
                await _requestRepository.AddRange(GetAll());
            }, 200);
        }

        public void Release(IncomingRequest item)
        {
            if (counter < MAX)
            {
                items.Add(item);
                counter++;
            }
        }

        public IncomingRequest Get()
        {
            IncomingRequest item;
            if (items.TryTake(out item))
                counter--;

            return item;
        }

        public List<IncomingRequest> GetAll()
        {
            var result = items.Take<IncomingRequest>(counter).ToList();
            items.Clear();
            counter = 0;
            return result;
        }
    }
}
