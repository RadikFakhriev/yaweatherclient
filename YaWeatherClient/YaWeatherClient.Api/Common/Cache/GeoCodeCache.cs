﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.Models;
using YaWeatherClient.Api.Enums;

namespace YaWeatherClient.Api.Common.Cache
{
    public class GeoCodeCache
    {
        private readonly IMemoryCache _cache;
        private const string GEO_CODE_CACHEKEY = "_GeoCode";

        public GeoCodeCache(IMemoryCache cache)
        {
            _cache = cache;
            Store(new Dictionary<City, GeoCoordinate>()
            {
                { City.Kaliningrad, new GeoCoordinate (54.710162, 20.510137) },
                { City.Moscow, new GeoCoordinate(55.753438, 37.624831) },
                { City.Krasnodar, new GeoCoordinate(45.035470, 38.975313) },
                { City.Orenburg, new GeoCoordinate(51.768205, 55.096964) },
                { City.SaintPeterburg, new GeoCoordinate(59.939099, 30.315877) }
            });
        }

        public GeoCoordinate GetByCity(City cityKey)
        {
            CoordinateCacheEntry storedCoordinates = default;
            
            if (_cache.TryGetValue(GEO_CODE_CACHEKEY, out storedCoordinates))
            {
                GeoCoordinate targetCoordinate = default;

                return storedCoordinates.Value.TryGetValue(cityKey, out targetCoordinate) ? targetCoordinate : null;
            }

            return null;
        }

        public void Store(Dictionary<City, GeoCoordinate> coordinates)
        {
            _cache.Set(GEO_CODE_CACHEKEY, new CoordinateCacheEntry(coordinates));
        }
    }
}
