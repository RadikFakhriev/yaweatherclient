﻿using Microsoft.Extensions.Caching.Memory;
using System;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Entities.Models;
using YaWeatherClient.Api.Enums;

namespace YaWeatherClient.Api.Common.Cache
{
    public class ReceivedWeatherCache
    {
        private readonly IMemoryCache _cache;
        private const string RECEIVED_WEATHER_CACHEKEY_PREFIX = "_ReceivedWeather";

        public ReceivedWeatherCache(IMemoryCache cache)
        {
            _cache = cache;
        }

        public YaWeatherResponseDTO GetByCity(City cityKey)
        {
            ReceivedWeatherCacheEntry receivedWeatherCacheEntry = default;
            string neededKey = $"{RECEIVED_WEATHER_CACHEKEY_PREFIX}_{cityKey}";

            if (_cache.TryGetValue(neededKey, out receivedWeatherCacheEntry))
            {
                if (DateTimeOffset.Now - receivedWeatherCacheEntry.Created >= receivedWeatherCacheEntry.ExpiresAfter)
                {
                    _cache.Remove(neededKey);
                    return null;
                }

                return receivedWeatherCacheEntry.Value?.yaWeather;
            }

            return null;
        }

        public void Store(ReceivedWeatherLine receivedWeather, TimeSpan expiresAfter)
        {
            _cache.Set($"{RECEIVED_WEATHER_CACHEKEY_PREFIX}_{receivedWeather.city}", new ReceivedWeatherCacheEntry(receivedWeather, expiresAfter));
        }
    }
}
