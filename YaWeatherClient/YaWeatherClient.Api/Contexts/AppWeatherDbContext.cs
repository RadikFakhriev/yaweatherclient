﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DB;

namespace YaWeatherClient.Api.Contexts
{
    public class AppWeatherDbContext : DbContext
    {
        public AppWeatherDbContext(DbContextOptions<AppWeatherDbContext> options) : base(options)
        {
        }

        public DbSet<IncomingRequest> IncomingRequests { get; set; }
    }
}
