﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Contexts
{
    public static class AppConnectionString
    {
        public static string ConnectionString { get; set; }
    }
}
