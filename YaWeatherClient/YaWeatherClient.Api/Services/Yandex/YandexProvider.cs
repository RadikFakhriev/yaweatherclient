﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Entities.Models;

namespace YaWeatherClient.Api.Services.Yandex
{
    public class YandexProvider
    {
        private readonly YandexApiHttpClientService _yandexApiHttpClient;

        public YandexProvider(YandexApiHttpClientService yandexApiHttpClient)
        {
            _yandexApiHttpClient = yandexApiHttpClient;
        }

        public async Task<YaWeatherResponseDTO> TestWeather(GeoCoordinate coordinate)
        {
            var response = await _yandexApiHttpClient.GetAsync<YaWeatherResponseDTO>("/v2/forecast", new Dictionary<string, string>()
            {
                { "lat", coordinate.latitude.ToString(CultureInfo.InvariantCulture) },
                { "lon", coordinate.longitude.ToString(CultureInfo.InvariantCulture) },
                { "lang", "ru_RU" }
            });

            return response;
        }
    }
}
