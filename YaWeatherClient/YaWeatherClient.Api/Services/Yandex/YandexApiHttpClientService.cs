﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.Models;
using YaWeatherClient.Api.Exceptions;

namespace YaWeatherClient.Api.Services.Yandex
{
    public class YandexApiHttpClientService
    {
        private readonly HttpClient _httpClient;

        public YandexApiHttpClientService(HttpClient httpClient, IOptions<YandexOptions> options)
        {
            httpClient.BaseAddress = new Uri(options.Value.Url);
            httpClient.DefaultRequestHeaders.Add("X-Yandex-API-Key", options.Value.Key);
            httpClient.Timeout = TimeSpan.FromMilliseconds(options.Value.Timeout);
            _httpClient = httpClient;
        }

        public async Task<TResult> GetAsync<TResult>(string url, IDictionary<string, string> queryString = default)
        {
            var requestUrl = queryString != default ? QueryHelpers.AddQueryString(url, queryString) : url;

            var response = await _httpClient.GetAsync(requestUrl);
            await CheckResponseAsync(response);

            var stream = await response.Content.ReadAsStreamAsync();

            if (stream is null || stream.CanRead is false)
            {
                return default;
            }

            return await JsonSerializer.DeserializeAsync<TResult>(stream, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });
        }

        private async Task CheckResponseAsync(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                throw new YandexApiHttpClientException(response.StatusCode, content);
            }
        }
    }
}
