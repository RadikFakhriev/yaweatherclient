﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using YaWeatherClient.Api.Contexts;
using YaWeatherClient.Api.Entities.DB;
using YaWeatherClient.Api.Interfaces;
using Z.Dapper.Plus;

namespace YaWeatherClient.Api.PostgreSQL.Repositories
{
    public class IncomingRequestRepository : IIncomingRequestRepository
    {
        public IncomingRequestRepository()
        {

        }

        public async Task AddAsync(IncomingRequest incRequest, CancellationToken cancellationToken)
        {
            using (NpgsqlConnection db = new NpgsqlConnection(AppConnectionString.ConnectionString))
            {
                var sqlQuery = "INSERT INTO public.\"IncomingRequests\"(\"Id\", \"Path\", \"UrlParams\", \"Status\", \"Method\", \"ResponseBody\") VALUES (@Id, @Path, @UrlParams, @Status, @Method, @ResponseBody)";
                await db.ExecuteAsync(sqlQuery, incRequest);
            }
        }

        public async Task UpdateReponseAsync(Guid requestId, int status, string responseBody, CancellationToken cancellationToken)
        {
            using (NpgsqlConnection db = new NpgsqlConnection(AppConnectionString.ConnectionString))
            {
                var sqlQuery = "UPDATE public.\"IncomingRequests\" SET \"Status\" = @status, \"ReponseBody\" = @responseBody WHERE \"Id\" = @requestId";
                await db.ExecuteAsync(sqlQuery, new { status, responseBody, requestId });
            }
        }

        public async Task InsertBulk(List<IncomingRequest> incRequests)
        {
            using (NpgsqlConnection db = new NpgsqlConnection(AppConnectionString.ConnectionString))
            {
                var sqlQuery = "INSERT INTO public.\"IncomingRequests\"(\"Id\", \"Path\", \"UrlParams\", \"Status\", \"Method\", \"ResponseBody\") VALUES (@Id, @Path, @UrlParams, @Status, @Method, @ResponseBody)";
                db.BulkInsert(incRequests);
            }
        }

        public async Task AddRange(List<IncomingRequest> incRequests)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppWeatherDbContext>();
            var _options = optionsBuilder.UseNpgsql(AppConnectionString.ConnectionString).Options;
            using (var _context = new AppWeatherDbContext(_options))
            {
                await _context.IncomingRequests.AddRangeAsync(incRequests);
                await _context.SaveChangesAsync();

                _context.ChangeTracker.Clear();
            }
        }
    }
}
