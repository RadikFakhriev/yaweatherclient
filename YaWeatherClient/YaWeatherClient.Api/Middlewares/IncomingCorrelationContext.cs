﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Middlewares
{
    public static class IncomingCorrelationContext
    {
        private static readonly AsyncLocal<string> CorrelationId = new AsyncLocal<string>();

        public static void SetCorrelationId()
        {
            Guid correlationId = Guid.NewGuid();

            if (!string.IsNullOrWhiteSpace(CorrelationId.Value))
            {
                throw new InvalidOperationException("Correlation id is already set");
            }

            CorrelationId.Value = correlationId.ToString();
        }

        public static Guid GetCorrelationId()
        {
            return Guid.Parse(CorrelationId.Value);
        }
    }
}
