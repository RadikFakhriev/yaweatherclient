﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using YaWeatherClient.Api.Common;
using YaWeatherClient.Api.Common.Cache;
using YaWeatherClient.Api.Contexts;
using YaWeatherClient.Api.Entities.DB;
using YaWeatherClient.Api.Interfaces;

namespace YaWeatherClient.Api.Middlewares
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;
        private readonly ObjectPool _pool;
        private readonly IIncomingRequestRepository _requestRepository;

        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger, ObjectPool pool,
                                        IIncomingRequestRepository requestRepository)
        {
            _next = next;
            _logger = logger;
            _pool = pool;
            _requestRepository = requestRepository;
            // TODO: ?? Microsoft.IO.RecyclableMemoryStream
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {

                IncomingCorrelationContext.SetCorrelationId();
                CancellationToken cancellationToken = context?.RequestAborted ?? CancellationToken.None;
                await LogRequest(context, cancellationToken);
                await LogResponse(context, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "RequestLoggingMiddleware для входящих запросов фронтенда");
            }
        }

        private async Task LogRequest(HttpContext context, CancellationToken cancellationToken)
        {
            string urlParams = context.Request?.QueryString.ToString();
            var correlationId = IncomingCorrelationContext.GetCorrelationId();
        }

        private async Task LogResponse(HttpContext context, CancellationToken cancellationToken)
        {
            var originalBody = context.Response.Body;
            string urlParams = context.Request?.QueryString.ToString();
            string bodyText = "";
            var correlationId = IncomingCorrelationContext.GetCorrelationId();

            using MemoryStream newBody = new MemoryStream();
            context.Response.Body = newBody;

            try
            {
                await _next(context);
            }
            finally
            {
                newBody.Seek(0, SeekOrigin.Begin);
                bodyText = await new StreamReader(context.Response.Body).ReadToEndAsync();

                _pool.Release(new IncomingRequest()
                {
                    Id = correlationId,
                    Path = context.Request?.Path,
                    UrlParams = urlParams,
                    Method = context.Request?.Method,
                    Status = context.Response.StatusCode,
                    ResponseBody = bodyText
                });

                newBody.Seek(0, SeekOrigin.Begin);
                await newBody.CopyToAsync(originalBody);
            }
        }
    }
}
