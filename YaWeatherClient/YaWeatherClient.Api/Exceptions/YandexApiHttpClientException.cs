﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Exceptions
{
    public class YandexApiHttpClientException : Exception
    {
        public HttpStatusCode? StatusCode { get; }
        public override string Message { get; }
        public string Content { get; }

        public YandexApiHttpClientException(HttpStatusCode statusCode, string content)
        {
            StatusCode = statusCode;
            Content = content;

            if (StatusCode == HttpStatusCode.NotFound)
                Message = $"Не удалось получить ответ от сервера: {StatusCode}({StatusCode:D}). Возможно неправильно указана ссылка";
            else
                Message = $"Не удалось получить ответ от сервера: {StatusCode}({StatusCode:D})";
        }

        public YandexApiHttpClientException(string message, string content, Exception innerException) : base(message, innerException)
        {
            Content = content;
        }
    }
}
