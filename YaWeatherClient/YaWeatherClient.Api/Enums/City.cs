﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Enums
{
    public enum City
    {
        Krasnodar,
        Moscow,
        Orenburg,
        SaintPeterburg,
        Kaliningrad
    }
}
