﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Entities.DTO
{
    public class AppWeatherResponseDTO
    {
        public int Temp { get; set; }
        public int FeelsLike { get; set; }
        public int? TempWater { get; set; }
        public string Condition { get; set; }
        public float WindSpeed { get; set; }
        public float WindGust { get; set; }
        public string WindDir { get; set; }
        public int PressureMm { get; set; }
        public string Season { get; set; }
        public long ObsTime { get; set; }
    }
}
