﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Entities.DTO
{
    public class YaWeatherResponseDTO
    {
        public long Now { get; set; }
        [JsonPropertyName("now_dt")]
        public DateTime NowDt { get; set; }
        public Fact fact { get; set; }
    }

    public class Fact
    {
        public int Temp { get; set; }
        [JsonPropertyName("feels_like")]
        public int FeelsLike { get; set; }
        [JsonPropertyName("temp_water")]
        public int? TempWater { get; set; }
        public string Condition { get; set; }
        [JsonPropertyName("wind_speed")]
        public float WindSpeed { get; set; }
        [JsonPropertyName("wind_gust")]
        public float WindGust { get; set; }
        [JsonPropertyName("wind_dir")]
        public string WindDir { get; set; }
        [JsonPropertyName("pressure_mm")]
        public int PressureMm { get; set; }
        public string Season { get; set; }
        [JsonPropertyName("obs_time")]
        public long ObsTime { get; set; }
    }
}
