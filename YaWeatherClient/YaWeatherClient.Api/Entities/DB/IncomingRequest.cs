﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Entities.DB
{
    public class IncomingRequest
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public string UrlParams { get; set; }
        public int Status { get; set; }
        public string Method { get; set; }
        public string ResponseBody { get; set; }
    }
}
