﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Enums;

namespace YaWeatherClient.Api.Entities.Models
{
    public class ReceivedWeatherCacheEntry
    {
        public ReceivedWeatherLine Value { get; }
        internal DateTimeOffset Created { get; } = DateTimeOffset.Now;
        internal TimeSpan ExpiresAfter { get; }

        public ReceivedWeatherCacheEntry(ReceivedWeatherLine value, TimeSpan expiresAfter)
        {
            Value = value;
            ExpiresAfter = expiresAfter;
        }
    }
}
