﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Entities.DTO;
using YaWeatherClient.Api.Enums;

namespace YaWeatherClient.Api.Entities.Models
{
    public class ReceivedWeatherLine
    {
        public City city { get; set; }
        public YaWeatherResponseDTO yaWeather { get; set; }
    }
}
