﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Entities.Models
{
    public class YandexOptions
    {
        public const string YandexApi = "Yandex.API";
        public string Url { get; set; }
        public string Key { get; set; }
        public double Timeout { get; set; }
        public double CacheLifeTime { get; set; }
    }
}
