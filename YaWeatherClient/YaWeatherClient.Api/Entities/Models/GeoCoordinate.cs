﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YaWeatherClient.Api.Entities.Models
{
    public class GeoCoordinate
    {
        public double latitude { get; set; }
        public double longitude { get; set; }

        public GeoCoordinate(double lat, double lon)
        {
            latitude = lat;
            longitude = lon;
        }
    }
}
