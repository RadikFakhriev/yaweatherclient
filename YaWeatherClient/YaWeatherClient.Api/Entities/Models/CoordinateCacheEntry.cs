﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YaWeatherClient.Api.Enums;

namespace YaWeatherClient.Api.Entities.Models
{
    public class CoordinateCacheEntry
    {
        public Dictionary<City, GeoCoordinate> Value { get; }

        public CoordinateCacheEntry(Dictionary<City, GeoCoordinate> value)
        {
            Value = value;
        }
    }
}
